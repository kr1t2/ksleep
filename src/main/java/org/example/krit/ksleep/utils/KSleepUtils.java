package org.example.krit.ksleep.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.example.krit.ksleep.KSleep;
import org.example.krit.ksleep.Scheduler;

public class KSleepUtils {
    public static void sendSleepMessageToEveryone() {
        String message = ChatColor.translateAlternateColorCodes('&', KSleep.getInstance().getConfig().getString("subtitle-sleep-message")
                .replace("<sleepCount>", String.valueOf(KSleep.sleepingPlayers))
                .replace("<needToSleepCount>", String.valueOf(KSleep.needToSleep)));

        for (Player people : Bukkit.getOnlinePlayers()) {
            people.sendActionBar(message);
        }
    }
}
