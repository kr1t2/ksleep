package org.example.krit.ksleep;

import org.bukkit.plugin.java.JavaPlugin;
import org.example.krit.ksleep.commands.KSleepCommand;
import org.example.krit.ksleep.commands.KSleepTABCompleter;
import org.example.krit.ksleep.listeners.PlayerJoinListener;
import org.example.krit.ksleep.listeners.PlayerSleepListener;
import org.example.krit.ksleep.tasks.CheckSleepingPlayers;

public final class KSleep extends JavaPlugin {
    private static KSleep plugin;

    public static int sleepingPlayers = 0;
    public static int needToSleep = 0;

    @Override
    public void onEnable() {
        plugin = this;

        // Сохранение конфига
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        // Прослушиватели событий
        getServer().getPluginManager().registerEvents(new PlayerSleepListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);

        // Команды
        getCommand("ksleep").setExecutor(new KSleepCommand());
        getCommand("ksleep").setTabCompleter(new KSleepTABCompleter());

        // Таймеры
        Scheduler.runTimer(new CheckSleepingPlayers(), 0, 1);
    }

    public static KSleep getInstance() {
        return plugin;
    }
}
