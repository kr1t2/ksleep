package org.example.krit.ksleep.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.example.krit.ksleep.KSleep;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        KSleep.needToSleep = (int) Math.ceil((double) Bukkit.getOnlinePlayers().size() / KSleep.getInstance().getConfig().getInt("skipCount"));
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        KSleep.needToSleep = (int) Math.ceil((double) Bukkit.getOnlinePlayers().size() / KSleep.getInstance().getConfig().getInt("skipCount"));
    }
}
