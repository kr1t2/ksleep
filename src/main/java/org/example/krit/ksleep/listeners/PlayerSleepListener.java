package org.example.krit.ksleep.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.example.krit.ksleep.KSleep;
import org.example.krit.ksleep.utils.KSleepUtils;

public class PlayerSleepListener implements Listener {

    @EventHandler
    public void onPlayerSleep(PlayerBedEnterEvent e) {
        if (e.getBedEnterResult() != PlayerBedEnterEvent.BedEnterResult.OK) {
            return;
        }
        KSleepUtils.sendSleepMessageToEveryone();
        KSleep.sleepingPlayers++;


    }
    @EventHandler
    public void onBedLeave(PlayerBedLeaveEvent e) {
        KSleep.sleepingPlayers--;
        KSleepUtils.sendSleepMessageToEveryone();
    }
}
