package org.example.krit.ksleep.tasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.example.krit.ksleep.KSleep;

import java.util.Objects;

public class CheckSleepingPlayers implements Runnable {
    @Override
    public void run() {
        String worldName = KSleep.getInstance().getConfig().getString("world-name");
        if (KSleep.sleepingPlayers >= Math.ceil((float) KSleep.needToSleep / KSleep.getInstance().getConfig().getInt("skipCount")) && !(Bukkit.getWorld(worldName).getTime() < 12516)) {
            Bukkit.getWorld(worldName).setTime(Bukkit.getWorld(worldName).getTime() + KSleep.getInstance().getConfig().getInt("skipSpeed"));

            if (Bukkit.getWorld(worldName).getTime() < 12516) {
                KSleep.sleepingPlayers = 0;
                for (Player people : Bukkit.getOnlinePlayers()) {
                    people.sendMessage(ChatColor.translateAlternateColorCodes('&', Objects.requireNonNull(KSleep.getInstance().getConfig().getString("night-skip-message"))));
                }
            }
        }


    }
}
