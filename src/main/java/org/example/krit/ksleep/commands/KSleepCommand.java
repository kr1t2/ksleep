package org.example.krit.ksleep.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.example.krit.ksleep.KSleep;
import org.jetbrains.annotations.NotNull;

public class KSleepCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 0) {
            return true;
        }
        switch (args[0]) {
            case "reload":
                if (sender instanceof Player p) {
                    if (!p.hasPermission("ksleep.reload")) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', KSleep.getInstance().getConfig().getString("no-permission-message")));
                        return true;
                    }
                } else if (sender instanceof BlockCommandSender) {
                    return true;
                }
                KSleep.getInstance().saveConfig();

                break;
            case "skip":
                if (sender instanceof Player p) {
                    if (!p.hasPermission("ksleep.skip")) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', KSleep.getInstance().getConfig().getString("no-permission-message")));
                        return true;
                    }
                    KSleep.sleepingPlayers = KSleep.needToSleep;
                }
                break;
            default:
                if (sender instanceof Player p) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6KSleep &r- Команда: &e/ksleep skip/reload"));
                }
        }

        return true;
    }
}
