package org.example.krit.ksleep.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class KSleepTABCompleter implements TabCompleter {
    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1) {
            String[]commands = {"reload", "skip"};

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[0], List.of(commands), completions);
            return completions;
        }
        return null;
    }
}
